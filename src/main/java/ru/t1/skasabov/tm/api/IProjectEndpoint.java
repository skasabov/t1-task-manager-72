package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    List<ProjectDto> findAll();

    @NotNull
    @WebMethod
    ProjectDto save(@NotNull @WebParam(name = "project", partName = "project") ProjectDto project);

    @Nullable
    @WebMethod
    ProjectDto findById(@NotNull @WebParam(name = "id", partName = "id") String id);

    @WebMethod
    boolean existsById(@NotNull @WebParam(name = "id", partName = "id") String id);

    @WebMethod
    long count();

    @WebMethod
    void deleteById(@NotNull @WebParam(name = "id", partName = "id") String id);

    @WebMethod
    void delete(@NotNull @WebParam(name = "project", partName = "project") ProjectDto project);

    @WebMethod
    void deleteAll(@Nullable @WebParam(name = "projects", partName = "projects") List<ProjectDto> projects);

    @WebMethod
    void clear();

}
