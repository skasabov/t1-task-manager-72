package ru.t1.skasabov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Result {

    private boolean success;

    @Nullable
    private String message;

    public Result(final boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        this.message = e.getMessage();
    }

}
