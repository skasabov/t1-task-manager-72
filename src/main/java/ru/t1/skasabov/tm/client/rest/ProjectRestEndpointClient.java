package ru.t1.skasabov.tm.client.rest;

import feign.Feign;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.util.CookieManagerUtil;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

public interface ProjectRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        @NotNull final CookieManager cookieManager = CookieManagerUtil.cookieManager;
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        @NotNull final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return Feign.builder()
                .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @Nullable
    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @NotNull
    @PostMapping("/save")
    ProjectDto save(@NotNull @RequestBody ProjectDto project);

    @Nullable
    @GetMapping("/findById/{id}")
    ProjectDto findById(@NotNull @PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@NotNull @PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@NotNull @RequestBody ProjectDto project);

    @PostMapping("/deleteAll")
    void deleteAll(@Nullable @RequestBody List<ProjectDto> projects);

    @PostMapping("/clear")
    void clear();

}
