package ru.t1.skasabov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.t1.skasabov.tm.config.DatabaseConfiguration;
import ru.t1.skasabov.tm.config.WebApplicationConfiguration;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.marker.UnitCategory;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class ProjectControllerTest {

    @NotNull
    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    private boolean reqAuth = false;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @Before
    @SneakyThrows
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        authenticate();
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
        task1.setProjectId(project1.getId());
        task1.setUserId(UserUtil.getUserId());
        task2.setProjectId(project2.getId());
        task2.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() {
        if (reqAuth) {
            authenticate();
        }
        reqAuth = false;
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void createTest() {
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @SneakyThrows
    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void createNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteTest() {
        @NotNull final String url = "/project/delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteNullTest() {
        @NotNull final String url = "/project/delete/";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteInvalidTest() {
        @NotNull final String url = "/project/delete/some_id";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(2, projectRepository.countByUserId(UserUtil.getUserId()));
    }

    @SneakyThrows
    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void deleteNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        @NotNull final String url = "/project/delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void editGetTest() {
        @NotNull final String url = "/project/edit/" + project2.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @SneakyThrows
    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void editGetNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        @NotNull final String url = "/project/edit/" + project2.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void editGetNullTest() {
        @NotNull final String url = "/project/edit/";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void editGetInvalidTest() {
        @NotNull final String url = "/project/edit/some_id";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void editPostTest() {
        @NotNull final String url = "/project/edit/" + project1.getId();
        project1.setDescription("Test Project 1");
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .flashAttr("project", project1))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final ProjectDto project = projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.getName(), project.getName());
        Assert.assertEquals(project1.getDescription(), project.getDescription());
    }

    @SneakyThrows
    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void editPostNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        @NotNull final String url = "/project/edit/" + project1.getId();
        project1.setDescription("Test Project 1");
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .flashAttr("project", project1));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void editPostNullTest() {
        @NotNull final String url = "/project/edit/";
        mockMvc.perform(MockMvcRequestBuilders.post(url))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @SneakyThrows
    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void editPostInvalidTest() {
        @NotNull final String url = "/project/edit/some_id";
        mockMvc.perform(MockMvcRequestBuilders.post(url));
    }

}
