package ru.t1.skasabov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.t1.skasabov.tm.config.DatabaseConfiguration;
import ru.t1.skasabov.tm.config.WebApplicationConfiguration;
import ru.t1.skasabov.tm.dto.Result;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.UnitCategory;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class AuthEndpointTest {

    @NotNull
    private static final String AUTH_URL = "http://localhost:8080/api/auth/";

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        login("test", "test");
    }

    @After
    @SneakyThrows
    public void clean() {
        logout();
    }

    @NotNull
    @SneakyThrows
    private Result login(@NotNull final String login, @NotNull final String password) {
        @NotNull final String url = AUTH_URL + "login";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .param("login", login)
                        .param("password", password)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Result.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void loginTest() {
        SecurityContextHolder.getContext().setAuthentication(null);
        Assert.assertTrue(login("admin", "admin").isSuccess());
    }

    @Test
    @Category(UnitCategory.class)
    public void loginEmptyLoginTest() {
        SecurityContextHolder.getContext().setAuthentication(null);
        Assert.assertFalse(login("", "admin").isSuccess());
    }

    @Test
    @Category(UnitCategory.class)
    public void loginEmptyPasswordTest() {
        SecurityContextHolder.getContext().setAuthentication(null);
        Assert.assertFalse(login("admin", "").isSuccess());
    }

    @Test
    @Category(UnitCategory.class)
    public void loginInvalidLoginTest() {
        SecurityContextHolder.getContext().setAuthentication(null);
        Assert.assertFalse(login("user", "admin").isSuccess());
    }

    @Test
    @Category(UnitCategory.class)
    public void loginInvalidPasswordTest() {
        SecurityContextHolder.getContext().setAuthentication(null);
        Assert.assertFalse(login("admin", "user").isSuccess());
    }

    @Nullable
    @SneakyThrows
    private UserDto profile() {
        @NotNull final String url = AUTH_URL + "profile";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, UserDto.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void profileTest() {
        @Nullable final UserDto user = profile();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void profileNoAuthTest() {
        SecurityContextHolder.getContext().setAuthentication(null);
        profile();
    }

    @NotNull
    @SneakyThrows
    private Result logout() {
        @NotNull final String url = AUTH_URL + "logout";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Result.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void logoutTest() {
        Assert.assertFalse(logout().isSuccess());
    }

}
