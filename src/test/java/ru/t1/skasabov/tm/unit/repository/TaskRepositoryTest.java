package ru.t1.skasabov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.config.DatabaseConfiguration;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.marker.UnitCategory;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.List;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    private final TaskDto task3 = new TaskDto("Test Task 3");

    @NotNull
    private final TaskDto task4 = new TaskDto("Test Task 4");

    @NotNull
    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        task1.setProjectId(project1.getId());
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByEmptyIdTest() {
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), ""));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByInvalidIdTest() {
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task3.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByIdTest() {
        Assert.assertTrue(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), task2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByEmptyIdTest() {
        Assert.assertFalse(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), ""));
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByInvalidIdTest() {
        Assert.assertFalse(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), task4.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void countTest() {
        Assert.assertEquals(2, taskRepository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByIdTest() {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task1.getId());
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByEmptyIdTest() {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), "");
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByInvalidIdTest() {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task3.getId());
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteAllTest() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByProjectIdTest() {
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project1.getId());
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByEmptyProjectIdTest() {
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), "");
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByInvalidProjectIdTest() {
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project2.getId());
        @Nullable final List<TaskDto> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

}
