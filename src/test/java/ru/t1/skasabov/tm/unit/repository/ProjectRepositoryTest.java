package ru.t1.skasabov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.config.DatabaseConfiguration;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.marker.UnitCategory;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    @NotNull
    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    @NotNull
    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        project4.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
        task1.setProjectId(project1.getId());
        task1.setUserId(UserUtil.getUserId());
        task2.setProjectId(project2.getId());
        task2.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByEmptyIdTest() {
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), ""));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByInvalidIdTest() {
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project3.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByIdTest() {
        Assert.assertTrue(projectRepository.existsByUserIdAndId(UserUtil.getUserId(), project2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByEmptyIdTest() {
        Assert.assertFalse(projectRepository.existsByUserIdAndId(UserUtil.getUserId(), ""));
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByInvalidIdTest() {
        Assert.assertFalse(projectRepository.existsByUserIdAndId(UserUtil.getUserId(), project4.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void countTest() {
        Assert.assertEquals(2, projectRepository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByIdTest() {
        taskRepository.deleteById(task1.getId());
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project1.getId());
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByEmptyIdTest() {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), "");
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByInvalidIdTest() {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project3.getId());
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteAllTest() {
        taskRepository.deleteAll(Arrays.asList(task1, task2));
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(0, projects.size());
    }

}
