package ru.t1.skasabov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.t1.skasabov.tm.config.DatabaseConfiguration;
import ru.t1.skasabov.tm.config.WebApplicationConfiguration;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.marker.UnitCategory;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class TaskEndpointTest {

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    private final TaskDto task3 = new TaskDto("Test Task 3");

    @NotNull
    private final TaskDto task4 = new TaskDto("Test Task 4");

    private boolean reqAuth = false;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        authenticate();
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        save(task1);
        save(task2);
    }

    @After
    @SneakyThrows
    public void clean() {
        if (reqAuth) {
            authenticate();
        }
        reqAuth = false;
        clear();
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @SneakyThrows
    private void save(@NotNull final TaskDto task) {
        @NotNull final String url = TASK_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk());
    }

    @Test
    @Category(UnitCategory.class)
    public void saveTest() {
        save(task3);
        @Nullable final TaskDto task = findById(task3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task3.getId());
    }

    @SneakyThrows
    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void saveNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        save(task3);
    }

    @NotNull
    @SneakyThrows
    private List<TaskDto> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDto[].class));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByAllTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void findAllNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        findAll();
    }

    @Nullable
    @SneakyThrows
    private TaskDto findById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDto.class);
    }

    @SneakyThrows
    private void findById() {
        @NotNull final String url = TASK_URL + "findById/";
        mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(findById(task1.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void findByIdNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        findById(task1.getId());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findByEmptyIdTest() {
        findById();
    }

    @Test
    @Category(UnitCategory.class)
    public void findByInvalidIdTest() {
        Assert.assertNull(findById(task3.getId()));
    }

    @SneakyThrows
    private boolean existsById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "existsById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Boolean.class);
    }

    @SneakyThrows
    private void existsById() {
        @NotNull final String url = TASK_URL + "existsById/";
        mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByIdTest() {
        Assert.assertTrue(existsById(task2.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void existsByIdNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        existsById(task2.getId());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void existsByEmptyIdTest() {
        existsById();
    }

    @Test
    @Category(UnitCategory.class)
    public void existsByInvalidIdTest() {
        Assert.assertFalse(existsById(task4.getId()));
    }

    @SneakyThrows
    private long count() {
        @NotNull final String url = TASK_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Long.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void countTest() {
        Assert.assertEquals(2, count());
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void countNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        count();
    }

    @SneakyThrows
    private void deleteById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "deleteById/" + id;
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk());
    }

    @SneakyThrows
    private void deleteById() {
        @NotNull final String url = TASK_URL + "deleteById/";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByIdTest() {
        deleteById(task1.getId());
        Assert.assertNull(findById(task1.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void deleteByIdNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        deleteById(task1.getId());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteByEmptyIdTest() {
        deleteById();
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByInvalidIdTest() {
        deleteById(task3.getId());
        Assert.assertEquals(2, count());
    }

    @SneakyThrows
    private void delete(@NotNull final TaskDto task) {
        @NotNull final String url = TASK_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteTest() {
        delete(task2);
        Assert.assertNull(findById(task2.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void deleteNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        delete(task2);
    }

    @SneakyThrows
    private void deleteAll(@NotNull final List<TaskDto> tasks) {
        @NotNull final String url = TASK_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk());
    }

    @SneakyThrows
    private void deleteAll() {
        @NotNull final String url = TASK_URL + "deleteAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(null);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteAllTest() {
        deleteAll(Arrays.asList(task1, task2));
        Assert.assertNull(findById(task1.getId()));
        Assert.assertNull(findById(task2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteAllEmptyTest() {
        deleteAll(Collections.emptyList());
        Assert.assertEquals(2, count());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteAllNullTest() {
        deleteAll();
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void deleteAllNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        deleteAll(Arrays.asList(task1, task2));
    }

    @SneakyThrows
    private void clear() {
        @NotNull final String url = TASK_URL + "clear";
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        clear();
        Assert.assertEquals(0, findAll().size());
    }

    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void clearNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        clear();
    }
    
}
